﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerController : MonoBehaviour {
    //test vars
    //private bool facingRight;
    private Rigidbody2D rb;
    private Animator anim;
    [SerializeField] 
    private float speed;

    //player jumping variables [obsolete]
    public float fallMultiplier = -2.5f;
    public float lowJumpMultiplier;
    [Range(0, 10)]
    public float jumpVelocity;

    //boundary variables
    public float xMin, xMax, yMin, yMax;

    //player state variables
    public bool canJump;
    public bool isDrunk;
    public bool isTequilaDrunk;
    public bool isWineDrunk;

    public float buffDuration;
    public float immuneDuration;

    public int health;
    public bool healthChanged;

    public GameObject signGun; //child of PlayerController

    //player shooting variables
    public GameObject shot;
    public Transform shotSpawn;
    private float nextFire;
    public float fireRate;

    //Time variables
    public float slowdownFactor;
    public float slowdownLength;

    [FMODUnity.EventRef]
    public string player_shoot = "event:/playerShoot";   // string reference to the FMOD-authored Event named "playerShoot"; name will appear in the Unity Inspector
    FMOD.Studio.EventInstance player_shootEv;      // Unity EventInstance name for Jump event that was created in FMOD

    void Awake()
    {
        anim = GetComponent<Animator>();
    }
    /*void Flip()
    {
        // Switch the way the player is labelled as facing
        facingRight = !facingRight;

        // Multiply the player's x local scale by -1
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }*/
    void Start () {
        //Flip();
        rb = GetComponent<Rigidbody2D>(); //assigning the reference
        canJump = true;
	}


    public void IncreaseFireRate()
    {
        fireRate = 0.25f;
    }

    public void ResetFireRate()
    {
        fireRate = 0.40f;
    }

    public void SlowTime()
    {
        Time.timeScale = slowdownFactor;
        Time.fixedDeltaTime = Time.timeScale * 0.02f;
    }

    public void ResetTime()
    {
        Time.timeScale = 1;
    }

    void Update()
    {
        

        if (!isDrunk && (Input.GetButton("Fire1") && Time.time > nextFire))
        {
            // These lines should be pasted into the section where you want to cue/play the sound 
            player_shootEv = FMODUnity.RuntimeManager.CreateInstance(player_shoot);    // EventInstance is linked to the Event 
            player_shootEv.start();                                                    // FINALLY... Play the sound!!

            nextFire = Time.time + fireRate;
            Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
        }

        if (isDrunk && (Input.GetButton("Fire2") && Time.time > nextFire))
        {
            // These lines should be pasted into the section where you want to cue/play the sound 
            player_shootEv = FMODUnity.RuntimeManager.CreateInstance(player_shoot);    // EventInstance is linked to the Event 
            player_shootEv.start();                                                    // FINALLY... Play the sound!!

            nextFire = Time.time + fireRate;
            Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
        }

        if (isDrunk)
        {
            BuffCountdown();
        }

        if (healthChanged)
        {
            ImmuneCountdown();
        }

        /*if (!isDrunk && Input.GetButtonDown("Jump") && canJump && rb.velocity.y == 0) //JUMP functionality
        {
            rb.velocity = Vector2.up * jumpVelocity;
            canJump = false;
        }

        else if (isDrunk && Input.GetButtonDown("InvertJump") && canJump && rb.velocity.y ==0) 
        {
            rb.velocity = Vector2.up * jumpVelocity;
            canJump = false;
        }*/

        if (rb.velocity.y<0) //player is falling
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier -1) * Time.deltaTime; //gravity's y value
        }
    }

	void FixedUpdate ()
    {
        if (isDrunk)
        {
            float invertHorizontal = Input.GetAxis("InvertHorizontal");
            anim.SetBool("isMoving", true);
            MovePlayer(invertHorizontal);
        }

        else
        {
            float horizontal = Input.GetAxis("Horizontal");
            anim.SetBool("isMoving", true);
            MovePlayer(horizontal);
        }

        if (rb.velocity.x <=1)
            anim.SetBool("isMoving", false);

        rb.position = new Vector3
        (
            Mathf.Clamp(rb.position.x, xMin, xMax),
            Mathf.Clamp(rb.position.y, yMin, yMax),
            0.0f
        );
        
        
	}

    void MovePlayer(float invertHorizontal)
    {
        rb.velocity = new Vector2(invertHorizontal * speed, rb.velocity.y); //vector with an x val of -1 and y val of 0
        
    }
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Platform"))
        {
            canJump = true;
            //Debug.Log("canJump set to " + canJump);
        }

        

        if ((immuneDuration >0 && immuneDuration < 3) && other.gameObject.CompareTag("Enemy")) //ignore collisions w/enemy
        {
            Physics2D.IgnoreCollision(this.GetComponent<Collider2D>(), other.collider);
        }

        if (other.gameObject.CompareTag("Bullet"))
        {
            Physics2D.IgnoreCollision(this.GetComponent<Collider2D>(), other.collider);

        }
    }

    public void ImmuneCountdown()
    {
        if (isTequilaDrunk && immuneDuration >0)
        {
            immuneDuration -= Time.deltaTime;
            Debug.Log("Immunity countdown: " + immuneDuration);
        }

        if (isWineDrunk && immuneDuration > 0)
        {
            immuneDuration -= Time.fixedDeltaTime;
            Debug.Log("Immunity countdown: " + immuneDuration);
        }

        else
        {
            immuneDuration -= Time.deltaTime;
            Debug.Log("Immunity countdown: " + immuneDuration);
        }

        if (immuneDuration <= 0) //reset value when player gets hit
        {
            healthChanged = false;
            ResetImmuneDuration();
        }
    }

    void ResetImmuneDuration()
    {
        immuneDuration = 3f;
    }

    void ResetBuffDuration()
    {
        buffDuration = 12f;
    }

    void BuffCountdown()
    {
        anim.SetBool("isDrunk", true);

        if (isDrunk && isTequilaDrunk && buffDuration >0)
        {
            buffDuration -= Time.deltaTime;
            //Debug.Log(buffDuration);
        }
        if (isDrunk && isWineDrunk && buffDuration > 0)
        {
            buffDuration -= (Time.fixedDeltaTime* 1.67f);
            //Debug.Log(buffDuration);
        }

        if (buffDuration <= 0 && isTequilaDrunk)
        {
            anim.SetBool("isDrunk", false);
            isDrunk = false;
            isTequilaDrunk = false;
            ResetFireRate();
            ResetBuffDuration();
            Debug.Log("isDrunk set to "+ isDrunk);
            Debug.Log("isTequilaDrunk set to " + isTequilaDrunk);
        }

        if (buffDuration <= 0 && isWineDrunk)
        {
            anim.SetBool("isDrunk", false);
            isDrunk = false;
            isWineDrunk = false;
            ResetTime(); //temp method. Might not work this way
            ResetBuffDuration();
            
            Debug.Log("isDrunk set to " + isDrunk);
            Debug.Log("isWineDrunk set to " + isWineDrunk);
        }
    }
}
