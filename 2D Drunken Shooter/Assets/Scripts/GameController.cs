﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class GameController : MonoBehaviour
{
    
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI gameOverText;
    public Slider healthBar;
    public int score;

    //Wave Coroutine vars
    public Transform[] spawnPoints;
    private int randomSpawnPoint;
    public GameObject zombie;
    public Vector2 spawnValues; //hold the random transform values for each newly generated zombie
    public int zombieCount; //holds # of times we cycle through the loop
    public float spawnWait; //hold our wait time value
    public float startWait; //time before waves begin
    public float waveWait;

    //Powerup Coroutine vars
    public GameObject [] alcohols;
    
    
    public int alcoholCount; //holds # of times we cycle through the loop
    public float spawnWaitAlcohol; //hold our wait time value
    public float startWaitAlcohol; //time before waves begin
    public float waveWaitAlcohol;

    //Care Package Coroutine vars
    public GameObject[] carePackages;
    public float startWaitCP;
    public float spawnWaitCP;
    public float waveWaitCP;
    

    private PlayerController playerController;
    public float loopDuration;

    [FMODUnity.EventRef]
    public string main_theme = "event:/mainThemeLoop";   // string reference to the FMOD-authored Event named "mainTheme"; name will appear in the Unity Inspector
    FMOD.Studio.EventInstance main_themeEv;      // Unity EventInstance name for maintheme event that was created in FMOD

    [FMODUnity.EventRef]
    public string zombie_FX = "event:/zombieFX";   // string reference to the FMOD-authored Event named "zombieFX"; name will appear in the Unity Inspector
    FMOD.Studio.EventInstance zombie_FXEv;      // Unity EventInstance name for zombieFX event that was created in FMOD

    public void UpdateHealthBar()
    {
        healthBar.value -= 0.333f; //player has 3 health
    }
    public void IncreaseHealthBar()
    {
        healthBar.value += 0.333f;
    }

    void Start()
    {
        gameOverText.text = "";
        StartCoroutine(SpawnWaves());
        StartCoroutine(SpawnAlcohol());
        StartCoroutine(SpawnGemOrHealth());

        // These lines should be pasted into the section where you want to cue/play the sound 
        main_themeEv = FMODUnity.RuntimeManager.CreateInstance(main_theme);    // EventInstance is linked to the Event 
        main_themeEv.start();                                                    // FINALLY... Play the sound!!

        // These lines should be pasted into the section where you want to cue/play the sound 
        zombie_FXEv = FMODUnity.RuntimeManager.CreateInstance(zombie_FX);    // EventInstance is linked to the Event 
        zombie_FXEv.start();                                                    // FINALLY... Play the sound!!

    }

    void Awake()
    {
        playerController = GameObject.FindObjectOfType<PlayerController>();
    }

    void Update()
    {
        if (playerController.isDrunk)
        {
            StartCoroutine(Pause());
        }
    }

    IEnumerator Pause()
    {
        main_themeEv.setPaused(true);
        yield return new WaitForSeconds(13.25f);  // Wait loopDuration
        main_themeEv.setPaused(false);                                      // Do something else
    }


    public void UpdateScore()
    {
        //can't tween TextMeshPro w/out pro version :(
        scoreText.text = "Score: " + score;
    }

    IEnumerator SpawnWaves()
    {
        while (true)
        {
            for (int i = 0; i < zombieCount; i++) //type 'for' and hit tab twice to autofill its contents
            {
                //Vector2 spawnPosition = new Vector2(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y);
                randomSpawnPoint = Random.Range(0, spawnPoints.Length);
                //allows values to be set in the inspector
                Quaternion spawnRotation = Quaternion.identity; //sets rotation value to 0 essentially
                Instantiate(zombie, spawnPoints[randomSpawnPoint].position, spawnRotation);
                yield return new WaitForSeconds(spawnWait);//time between hazard spawns
            }
            yield return new WaitForSeconds(waveWait);
        }
    }

    IEnumerator SpawnAlcohol()
    {
        yield return new WaitForSeconds(startWaitAlcohol);
        while (true)
        {
            for (int i = 0; i < alcoholCount; i++) //type 'for' and hit tab twice to autofill its contents
            {
                GameObject alcohol = alcohols[Random.Range(0, alcohols.Length)]; //picks a random power up
                Vector2 spawnPosition = new Vector2(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y);
                //allows values to be set in the inspector
                Quaternion spawnRotation = Quaternion.identity; //sets rotation value to 0 essentially
                Instantiate(alcohol, spawnPosition, spawnRotation);
                yield return new WaitForSeconds(spawnWaitAlcohol);//time between hazard spawns
            }
            yield return new WaitForSeconds(waveWaitAlcohol);
        }
    }
    IEnumerator SpawnGemOrHealth()
    {
        yield return new WaitForSeconds(startWaitCP);
        while (true)
        {
            for (int i = 0; i < alcoholCount; i++) //type 'for' and hit tab twice to autofill its contents
            {
                GameObject carePackage = carePackages[Random.Range(0, carePackages.Length)]; //picks a random power up
                Vector2 spawnPosition = new Vector2(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y);
                //allows values to be set in the inspector
                Quaternion spawnRotation = Quaternion.identity; //sets rotation value to 0 essentially
                Instantiate(carePackage, spawnPosition, spawnRotation);
                yield return new WaitForSeconds(spawnWaitCP);//time between hazard spawns
            }
            yield return new WaitForSeconds(waveWaitCP);
        }
    }
}

