﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TequilaEffects : MonoBehaviour {

    
    private PlayerController playerController;
    //private GameController gameController;

    [FMODUnity.EventRef]
    public string tequila_loop = "event:/tequilaLoop";   // string reference to the FMOD-authored Event named "tequilaLoop"; name will appear in the Unity Inspector
    FMOD.Studio.EventInstance tequila_loopEv;      // Unity EventInstance name for Jump event that was created in FMOD

    void Awake()
    {
        playerController = GameObject.FindObjectOfType<PlayerController>(); //to access PlayerController's vars
        //gameController = GameObject.FindObjectOfType<GameController>(); //access GameController's vars
    }

   
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            
            // These lines should be pasted into the section where you want to cue/play the sound 
            tequila_loopEv = FMODUnity.RuntimeManager.CreateInstance(tequila_loop);    // EventInstance is linked to the Event 
            tequila_loopEv.start();                                                    // FINALLY... Play the sound!!

            playerController.isDrunk = true; //set to Drunk
            playerController.isTequilaDrunk = true;
            playerController.IncreaseFireRate(); //increase player's Fire Rate

            Destroy(this.gameObject);
        }

        if (other.gameObject.CompareTag("Platform") || other.gameObject.CompareTag("Enemy") || other.gameObject.CompareTag("Bullet"))
        {
            Physics2D.IgnoreCollision(this.GetComponent<Collider2D>(), other.collider);
        }
        
    }

}
