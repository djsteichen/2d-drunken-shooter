﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DestroyByContact : MonoBehaviour {

    private PlayerController playerController;
    private GameController gameController;
    private Animator anim;

    [FMODUnity.EventRef]
    public string player_hurt = "event:/playerHurt";   // string reference to the FMOD-authored Event named "playerHurt"; name will appear in the Unity Inspector
    FMOD.Studio.EventInstance player_hurtEv;      // Unity EventInstance name for playerHurt event that was created in FMOD

    [FMODUnity.EventRef]
    public string zombie_death = "event:/zombieDeath";   // string reference to the FMOD-authored Event named "zombieDeath"; name will appear in the Unity Inspector
    FMOD.Studio.EventInstance zombie_deathEv;      // Unity EventInstance name for zombieDeath event that was created in FMOD

    [FMODUnity.EventRef]
    public string game_over = "event:/gameOver";   // string reference to the FMOD-authored Event named "gameOver"; name will appear in the Unity Inspector
    FMOD.Studio.EventInstance game_overEv;      // Unity EventInstance name for gameOver event that was created in FMOD


    // Use this for initialization
    void Awake () {
        playerController = GameObject.FindObjectOfType<PlayerController>(); //to access PlayerController's vars
        gameController = GameObject.FindObjectOfType<GameController>(); //access GameController's vars
        anim = GetComponent<Animator>();

    }

    void Start ()
    {
      
    }

    // Update is called once per frame
    void Update () {
		
	}

    void OnCollisionEnter2D(Collision2D other)
    {
        //how to fix objects colliding with themselves?
        if (other.gameObject.CompareTag("Player"))
        {
            
                                                             
            if (playerController.immuneDuration == 3) //when player is not invincible
            {
                player_hurtEv = FMODUnity.RuntimeManager.CreateInstance(player_hurt);    // EventInstance is linked to the Event 
                player_hurtEv.start();
                Camera.main.DOShakePosition(0.3f, 3f, 75);
                playerController.ImmuneCountdown(); //starts invincibility timer upon getting hit
                playerController.health--; //reduces player health
                gameController.UpdateHealthBar(); //update healthBar UI
                playerController.healthChanged = true;
                anim.SetTrigger("PlayerCollision");//set Zombie animation to Attack
                
            }

            
            
            if (playerController.health <= 0)
            {
                game_overEv = FMODUnity.RuntimeManager.CreateInstance(game_over);    // EventInstance is linked to the Event 
                game_overEv.start();

                gameController.gameOverText.text = "Game Over";
                gameController.StopAllCoroutines();
                Destroy(other.gameObject); //destroy player when health is 0
            }
        }

        if (other.gameObject.CompareTag("Enemy"))
        {
            Physics2D.IgnoreCollision(this.GetComponent<Collider2D>(), other.collider);

        }

        if (other.gameObject.CompareTag("Bullet"))
        {
            zombie_deathEv = FMODUnity.RuntimeManager.CreateInstance(zombie_death);    // EventInstance is linked to the Event 
            zombie_deathEv.start();

            Destroy(other.gameObject);
            Destroy(this.gameObject);
            gameController.score += 100; //increase score
            gameController.UpdateScore(); //update score text obj
        }

        else
            return;
    }
}
