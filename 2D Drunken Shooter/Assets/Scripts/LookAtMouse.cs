﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtMouse : MonoBehaviour {

    void FaceMouse() //look towards mousePosition
    {

        Vector3 mousePos = Input.mousePosition;
        mousePos = Camera.main.ScreenToWorldPoint(mousePos);
        Vector2 direction = new Vector2(mousePos.x - transform.position.x, mousePos.y - transform.position.y);

        transform.right = direction;
        /*if (mousePos.x <0)
        {
            this.transform.rotation = Quaternion.Euler(direction.x, -180, 0);
        }
        else
        {
            this.transform.rotation = Quaternion.Euler(direction.x, 0, 0);
        }*/
    }

    void Update()
    {
        FaceMouse();
    }

    
}
