﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthDrop : MonoBehaviour {
    private PlayerController playerController;
    private GameController gameController;
	// Use this for initialization
	void Awake () {
        playerController = GameObject.FindObjectOfType<PlayerController>();
        gameController = GameObject.FindObjectOfType<GameController>();
    }

    // Update is called once per frame
    void Update () {
		
	}
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (playerController.health == 3) //player at max health
            {
                gameController.score += 500;
                gameController.UpdateScore();
                Destroy(this.gameObject);
            }

            else
            {
                playerController.health++;
                gameController.IncreaseHealthBar();
                Destroy(this.gameObject);
            }
            
        }

        if (other.gameObject.CompareTag("Platform") || other.gameObject.CompareTag("Enemy") || other.gameObject.CompareTag("Bullet")) //not perfect - collides w/obj then ignores collision
        {
            //Debug.Log("enemy collision detected");
            Physics2D.IgnoreCollision(this.GetComponent<Collider2D>(), other.collider);
        }

    }
}
