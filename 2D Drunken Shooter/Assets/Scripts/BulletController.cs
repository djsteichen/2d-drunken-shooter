﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {

    public float speed;

    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(Vector2.up * speed * Time.deltaTime); //move immediately
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        //Debug.Log("bullet colliding with something");

        if (other.gameObject.CompareTag("Platform"))
        {
            Destroy(this.gameObject);
        }
    }
}
