﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class WineEffects : MonoBehaviour {

    private PlayerController playerController;
    //private GameController gameController;

    [FMODUnity.EventRef]
    public string wine_loop = "event:/wineLoop";   // string reference to the FMOD-authored Event named "wineLoop"; name will appear in the Unity Inspector
    FMOD.Studio.EventInstance wine_loopEv;      // Unity EventInstance name for Jump event that was created in FMOD

    void Awake()
    {
        playerController = GameObject.FindObjectOfType<PlayerController>(); //to access PlayerController's vars
        //gameController = GameObject.FindObjectOfType<GameController>(); //access GameController's vars
    }


    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            // These lines should be pasted into the section where you want to cue/play the sound 
            wine_loopEv = FMODUnity.RuntimeManager.CreateInstance(wine_loop);    // EventInstance is linked to the Event 
            wine_loopEv.start();                                                    // FINALLY... Play the sound!!

            playerController.isDrunk = true; //set to Drunk
            playerController.isWineDrunk = true;
            playerController.SlowTime(); //slow down time
            Camera.main.DOShakePosition(10f, 0.2f, 5);
            Destroy(this.gameObject);
        }

        if (other.gameObject.CompareTag("Platform") || other.gameObject.CompareTag("Enemy") || other.gameObject.CompareTag("Bullet")) //not perfect - collides w/obj then ignores collision
        {
            //Debug.Log("enemy collision detected");
            Physics2D.IgnoreCollision(this.GetComponent<Collider2D>(), other.collider);
        }
        
    }
}
