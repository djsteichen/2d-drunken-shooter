﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashMovement : MonoBehaviour
{
    private PlayerController playerController;

    private Rigidbody2D rb;
    public float dashSpeed;
    private float dashTime;
    public float startDashTime;
    private int direction;

    public GameObject dashEffect;

    [FMODUnity.EventRef]
    public string _jump = "event:/jump";   // string reference to the FMOD-authored Event named "jump"; name will appear in the Unity Inspector
    FMOD.Studio.EventInstance _jumpEv;      // Unity EventInstance name for playerHurt event that was created in FMOD

    void Awake()
    {
        playerController = GameObject.FindObjectOfType<PlayerController>(); //to access PlayerController's vars

    }

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        dashTime = startDashTime;
    }

    void Update()
    {
        if (direction == 0)
        {
            if ((!playerController.isDrunk) && playerController.canJump && (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))) //sober
            {
                _jumpEv = FMODUnity.RuntimeManager.CreateInstance(_jump);    // EventInstance is linked to the Event 
                _jumpEv.start();                                                    // FINALLY... Play the sound!!

                //Instantiate(dashEffect, transform.position, Quaternion.identity);
                Debug.Log("Input = W or UP");
                direction = 1;
                playerController.canJump = false;
            }
            else if (playerController.canJump && playerController.isDrunk && 
                ((Input.GetKeyDown(KeyCode.S)) || Input.GetKeyDown(KeyCode.DownArrow))) //drunk condition
            {
                //Instantiate(dashEffect, transform.position, Quaternion.identity);
                Debug.Log("Input = S or DOWN");
                direction = 1;
                playerController.canJump = false;
            }
        }

        else
        {
            if (dashTime <= 0)
            {
                direction = 0;
                dashTime = startDashTime;
                rb.velocity = Vector2.zero; //player velocity set to 0
            }
            else
            {
                dashTime -= Time.deltaTime;

                if (direction == 1) //dash upwards
                {
                    rb.velocity = Vector2.up * dashSpeed;
                }
            }
        }

    }
} 
