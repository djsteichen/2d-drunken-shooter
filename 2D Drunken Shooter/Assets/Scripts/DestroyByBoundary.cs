﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByBoundary : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Wine") || other.gameObject.CompareTag("Tequila"))
        {
            Debug.Log(other);
            Destroy(other.gameObject);
        }
        
        /*if (other.gameObject.CompareTag("Bullet"))
        {
            Debug.Log("bullet exited");
            Destroy(other.gameObject);
        }

        else
            return;*/
        
    }
}
