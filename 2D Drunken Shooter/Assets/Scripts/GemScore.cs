﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemScore : MonoBehaviour {
    private GameController gameController;

    // Use this for initialization
    void Awake () {
        gameController = GameObject.FindObjectOfType<GameController>(); //access GameController's vars

    }

    // Update is called once per frame
    void Update () {
		
	}
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            gameController.score += 1000;
            gameController.UpdateScore();
            Destroy(this.gameObject);
        }

        if (other.gameObject.CompareTag("Platform") || other.gameObject.CompareTag("Enemy") || other.gameObject.CompareTag("Bullet")) //not perfect - collides w/obj then ignores collision
        {
            //Debug.Log("enemy collision detected");
            Physics2D.IgnoreCollision(this.GetComponent<Collider2D>(), other.collider);
        }

    }
}
